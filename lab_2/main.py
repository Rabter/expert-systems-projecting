class Rule:
    def __init__(self, number, input_nodes, output_node):
        self.input_nodes = input_nodes
        self.output_node = output_node
        self.number = number


class HyperGraph:
    def __init__(self, rules):
        self.rules = rules

    def search_from_input(self, nodes, target):
        if target in nodes:
            return True, [], []
        found = False
        closed = nodes[:]
        closed_rules = list()
        applicable_exist = True
        while not found and applicable_exist:
            applicable_exist = False
            for rule in self.rules:
                if not (rule.number in closed_rules or rule.output_node in closed):
                    applicable_exist = True
                    applicable = True
                    for node in rule.input_nodes:
                        if node not in closed:
                            applicable = False
                    if applicable:
                        if target == rule.output_node:
                            found = True
                        else:
                            closed.append(rule.output_node)
                        closed_rules.append(rule.number)
        if found:
            return True, closed, closed_rules
        else:
            return False, [], []


if __name__ == "__main__":
    rules = list()
    rules.append(Rule(104, [8, 31], 3))
    rules.append(Rule(101, [1, 2], 3))
    rules.append(Rule(103, [5, 6], 4))
    rules.append(Rule(102, [3, 2, 4], 7))
    rules.append(Rule(107, [12, 13], 11))
    rules.append(Rule(106, [4, 10, 11], 9))
    rules.append(Rule(111, [18, 32], 9))
    rules.append(Rule(105, [7, 9], 14))
    rules.append(Rule(112, [19, 20], 34))
    rules.append(Rule(109, [16, 17], 15))
    rules.append(Rule(108, [34, 15], 33))
    rules.append(Rule(110, [9, 34], 14))

    graph = HyperGraph(rules)

    print(graph.search_from_input([5, 6, 10, 12, 13, 19, 20], 14))
