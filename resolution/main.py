import copy


class Disjunct:
    def __init__(self, atoms):
        self.atoms = atoms

    def __str__(self):
        text_atoms = ''
        for atom in self.atoms:
            text_atoms += atom.__str__() + ', '
        return '(' + text_atoms.rstrip(', ') + ')'

    def __repr__(self):
        return self.__str__()


class Atom:
    def __init__(self, negative, name):
        self.negative = negative
        self.name = name

    def __str__(self):
        if self.negative:
            return '!' + self.name
        else:
            return self.name

    def __repr__(self):
        return self.__str__()


def disjunction(dis1, dis2):
    excluded = list()
    found = False
    for atom1 in dis1.atoms:
        for atom2 in dis2.atoms:
            if atom1.name == atom2.name:
                if atom1.negative != atom2.negative:
                    excluded.append(atom1.name)
                    found = True
    if found:
        res_atoms = list()
        for atom in dis1.atoms + dis2.atoms:
            if atom.name not in excluded:
                res_atoms.append(atom)
                excluded.append(atom.name)
        return Disjunct(res_atoms)
    else:
        return None


def resolution(disjuncts, target):
    print("Даны дизъюнкты:", disjuncts, "и цель:", target)
    for atom in target.atoms:
        atom.negative = not atom.negative
        disjuncts.append(Disjunct([atom]))

    print("После преобразования цели полученные дизъюнкты:", disjuncts, end='\n\n')

    size = len(disjuncts)
    i = 0
    while i < size - 1:
        j = i + 1
        while j < size:
            print("Для пары ", disjuncts[i], ', ', disjuncts[j], sep='', end=' ')
            res = disjunction(disjuncts[i], disjuncts[j])
            if res is not None:
                if len(res.atoms):
                    disjuncts.append(res)
                    size += 1
                    print("найден новый дизъюнкт:", res)
                    print("Текущие дизъюнкты:", disjuncts)
                else:
                    print("найден пустой дизъюнкт")
                    return True
            else:
                print("дизъюнкт не найден")
            print()
            j += 1
        i += 1
    return False


if __name__ == "__main__":
    a = Atom(False, 'a')
    b = Atom(False, 'b')
    c = Atom(False, 'c')
    d = Atom(False, 'd')
    neg_c = Atom(True, 'c')
    neg_d = Atom(True, 'd')
    neg_a = Atom(True, 'a')
    neg_b = Atom(True, 'b')

    abcd = Disjunct([copy.deepcopy(a), copy.deepcopy(b), copy.deepcopy(c), copy.deepcopy(d)])
    anbc = Disjunct([copy.deepcopy(a), copy.deepcopy(neg_b), copy.deepcopy(c)])

    print(resolution([abcd, anbc], Disjunct([a, d])))
