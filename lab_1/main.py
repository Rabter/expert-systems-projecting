import copy


class Rule:
    def __init__(self, number, start, end):
        self.number = number
        self.start = start
        self.end = end
        self.used = False


class GraphSearch:
    def __init__(self, rules, start, dest):
        self.rules = rules
        self.opened = list()
        self.closed = list()
        self.found = False
        self.start = start
        self.dest = dest

    def depth_first_search(self):
        path = dict()
        self.opened.insert(0, self.start)
        while not self.found and self.start not in self.closed:
            current = self.opened[0]
            due_closing = True
            for rule in self.rules:
                if not rule.used and rule.start == current and rule.end not in self.opened + self.closed:
                    due_closing = False
                    rule.used = True
                    self.opened.insert(0, rule.end)
                    path[rule.end] = rule.start
                    if rule.end == self.dest:
                        self.found = True
            if due_closing:
                self.closed.append(self.opened.pop(0))
        res = list()
        if self.found:
            current = self.dest
            res.append(current)
            while current != self.start:
                current = path[current]
                res.insert(0, current)
        return self.found, res

    def breadth_first_search(self):
        path = dict()
        queue = list()
        self.opened.insert(0, self.start)
        queue.insert(0, self.start)
        while not self.found and self.start not in self.closed:
            current = queue.pop(0)
            due_closing = True
            for rule in self.rules:
                if not rule.used and rule.start == current and rule.end not in self.opened + self.closed:
                    due_closing = False
                    rule.used = True
                    self.opened.append(rule.end)
                    queue.append(rule.end)
                    path[rule.end] = rule.start
                    if rule.end == self.dest:
                        self.found = True
            if due_closing:
                self.closed.append(current)
        res = list()
        if self.found:
            current = self.dest
            res.append(current)
            while current != self.start:
                current = path[current]
                res.insert(0, current)
        return self.found, res


if __name__ == "__main__":
    rules = list()

    rules.append(Rule(0, 0, 1))
    rules.append(Rule(1, 1, 2))
    rules.append(Rule(2, 2, 3))
    rules.append(Rule(3, 3, 4))
    rules.append(Rule(4, 0, 4))
    rules.append(Rule(5, 1, 4))

    searcher_dfs = GraphSearch(rules, 0, 3)
    searcher_bfs = copy.deepcopy(searcher_dfs)

    print("Depth-first search:", searcher_dfs.depth_first_search())
    print("Breadth-first search:", searcher_bfs.breadth_first_search())
