class Rule:
    def __init__(self, input_nodes, output_node):
        self.input_nodes = input_nodes
        self.output_node = output_node


class HyperGraphSearcher:
    def __init__(self, rules):
        self.rules = rules
        self.proven_nodes = list()
        self.proven_rules = list()

    def search_from_target(self, nodes, target):
        self.proven_nodes = list(nodes)
        res = self.prove_node(target)
        res_proven_nodes = self.proven_nodes
        res_proven_rules = self.proven_rules
        self.proven_nodes = list()
        self.proven_rules = list()
        return res, res_proven_nodes, res_proven_rules

    def prove_rule(self, rule_num):
        if rule_num in self.proven_rules:
            return True
        for child in self.rules[rule_num].input_nodes:
            if not self.prove_node(child):
                return False
        self.proven_rules.append(rule_num)
        return True

    def prove_node(self, node):
        if node in self.proven_nodes:
            return True
        for num, rule in self.rules.items():
            if rule.output_node == node and self.prove_rule(num):
                self.proven_nodes.append(node)
                return True
        return False


if __name__ == "__main__":
    rules = dict()
    rules[104] = Rule([8, 31], 3)
    rules[101] = Rule([1, 2], 3)
    rules[103] = Rule([5, 6], 4)
    rules[102] = Rule([3, 2, 4], 7)
    rules[107] = Rule([12, 13], 11)
    rules[106] = Rule([4, 10, 11], 9)
    rules[111] = Rule([18, 32], 9)
    rules[105] = Rule([7, 9], 14)
    rules[112] = Rule([19, 20], 34)
    rules[109] = Rule([16, 17], 15)
    rules[108] = Rule([34, 15], 33)
    rules[110] = Rule([9, 34], 14)

    graph = HyperGraphSearcher(rules)

    print(graph.search_from_target([19, 20, 18, 32], 14))
