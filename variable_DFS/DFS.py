from atom import Atom
from unification import *


class Rule:
    def __init__(self, input_nodes, output_node):
        self.input_nodes = input_nodes
        self.output_node = output_node


class HyperGraphSearcher:
    def __init__(self, rules):
        self.rules = rules
        self.table = None
        self.proven_nodes = list()
        self.proven_rules = list()

    def search_from_target(self, nodes, target):
        self.table = Table()
        self.proven_nodes = list(nodes)
        res = self.prove_node(target)
        res_proven_nodes = self.proven_nodes
        res_proven_rules = self.proven_rules
        self.proven_nodes = list()
        self.proven_rules = list()
        return res, res_proven_nodes, res_proven_rules

    def prove_rule(self, rule_num):
        print("Trying to prove rule №", rule_num, ':', sep='')
        if rule_num in self.proven_rules:
            print("Rule", rule_num, "has already been proven")
            return True
        for child in self.rules[rule_num].input_nodes:
            if not self.prove_node(child):
                return False
        self.proven_rules.append(rule_num)
        return True

    def prove_node(self, node):
        print("Trying to prove node ", node, ':', sep='')
        for proven in self.proven_nodes:
            if unification(self.table, node, proven):
                print("Node", node, "has already been proven")
                return True
        for num, rule in self.rules.items():
            if unification(self.table, rule.output_node, node):
                print("Successful unification:", rule.output_node, node)
                if self.prove_rule(num):
                    self.proven_nodes.append(node)
                    print("Node", node, "is proven")
                    return True
        print("Node", node, "is proven to be False")
        return False


if __name__ == "__main__":

    c_A = Constant('A')
    c_B = Constant('B')
    c_C = Constant('C')
    c_D = Constant('D')

    v_x1 = Variable("x1")
    v_x2 = Variable("x2")
    v_x3 = Variable("x3")
    v_x4 = Variable("x4")
    v_x5 = Variable("x5")
    v_x6 = Variable("x6")

    node1 = Atom("A", [v_x1])
    node2 = Atom("W", [v_x2])
    node3 = Atom("S", [v_x1, v_x2, v_x3])
    node4 = Atom("H", [v_x3])
    node5 = Atom("C", [v_x1])

    node6 = Atom("M", [v_x4])
    node7 = Atom("O", [c_A, v_x4])
    node8 = Atom("S", [c_C, v_x4, c_B])

    node9 = Atom("M", [v_x5])
    node10 = Atom("W", [v_x5])

    node11 = Atom("E", [v_x6, c_D])
    node12 = Atom("H", [v_x6])

    rules = dict()
    rules[1] = Rule([node1, node2, node3, node4], node5)
    rules[2] = Rule([node6, node7], node8)
    rules[3] = Rule([node9], node10)
    rules[4] = Rule([node11], node12)

    graph = HyperGraphSearcher(rules)

    target = Atom("C", [c_C])
    given = [
        Atom("O", [c_A, c_B]),
        Atom("M", [c_B]),
        Atom("A", [c_C]),
        Atom("E", [c_B, c_D]),
    ]

    res, nodes, rules = graph.search_from_target(given, target)
    print()
    print("Result:", res)
    print("Proven nodes:", nodes)
    print("Proven rules:", rules)
    print("Variable table:", graph.table, sep='\n')
