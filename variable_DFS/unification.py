import copy


class Constant:
    def __init__(self, value):
        self.variable = False
        self.value = value

    def __str__(self):
        return self.value

    def __repr__(self):
        return self.__str__()


class Variable:
    def __init__(self, name):
        self.variable = True
        self.name = name

    def __str__(self):
        return self.name

    def __repr__(self):
        return self.__str__()


class Table:
    def __init__(self):
        self.variables = dict()
        self.links = dict()

    def equals_const(self, var, const):
        if var.name not in self.variables or self.variables[var.name] is None:
            self.variables[var.name] = const
        else:
            if self.variables[var.name].value != const.value:
                return False
        if const.value not in self.links:
            self.links[const.value] = {var.name}
        else:
            self.links[const.value].add(var.name)
        return True

    def equals_var(self, var1, var2):
        if var1.name not in self.variables and var2.name not in self.variables:
            # Связь между переменными не сохраняется, но мне лень это фиксить
            self.variables[var1.name] = self.variables[var2.name] = None
        elif var1.name not in self.variables:
            self.variables[var1.name] = self.variables[var2.name]
        elif var2.name not in self.variables:
            self.variables[var1.name] = self.variables[var2.name]
        elif self.variables[var1.name] != self.variables[var2.name]:
            return False
        return True

    def reset(self, other):
        self.variables = other.variables
        self.links = other.links

    def val(self, var):
        return self.variables[var.name]

    def var_links(self, var):
        return self.links[self.variables[var.name]]

    def __str__(self):
        res = ""
        for const in self.links.keys():
            res += str(self.links[const]) + ": " + str(const) + "\n"
        return res


def unification(table, p1, p2):
    if p1.name != p2.name:
        return False
    if len(p1.terminals) != len(p2.terminals):
        return False
    original = copy.deepcopy(table)
    for t1, t2 in zip(p1.terminals, p2.terminals):
        if t1.variable:
            if t2.variable:
                if not table.equals_var(t1, t2):
                    print("Variable ", t1.name, "value mismatched another variable ", t2.name, ":", table.val(t1),
                          " != ", table.val(t2), sep='')
                    table.reset(original)
                    return False
            else:
                if not table.equals_const(t1, t2):
                    print("Variable value mismatched constant:", t1.name, "=", table.val(t1),
                          "but the constant is", t2.value)
                    table.reset(original)
                    return False
        else:
            if t2.variable:
                if not table.equals_const(t2, t1):
                    print("Variable value mismatched constant:", t2.name, "=", table.val(t2),
                          "but the constant is", t1.value)
                    table.reset(original)
                    return False
            else:
                if t1.value != t2.value:
                    print("Mismatch constants:", t1.value, "!=", t2.value)
                    table.reset(original)
                    return False
    return True

