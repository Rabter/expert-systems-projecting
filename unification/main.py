class Atom:
    def __init__(self, name, terminals):
        self.name = name
        self.terminals = terminals


class Constant:
    def __init__(self, value):
        self.variable = False
        self.value = value


class Variable:
    def __init__(self, name):
        self.variable = True
        self.name = name


class Table:
    def __init__(self):
        self.variables = dict()
        self.links = dict()

    def equals_const(self, var, const):
        if var.name not in self.variables or self.variables[var.name] is None:
            self.variables[var.name] = const
        else:
            if self.variables[var.name].value != const.value:
                return False
        if const.value not in self.links:
            self.links[const.value] = {var.name}
        else:
            self.links[const.value].add(var.name)
        return True

    def equals_var(self, var1, var2):
        if var1.name not in self.variables and var2.name not in self.variables:
            self.variables[var1.name] = self.variables[var2.name] = None
        elif var1.name not in self.variables:
            self.variables[var1.name] = self.variables[var2.name]
        elif var2.name not in self.variables:
            self.variables[var1.name] = self.variables[var2.name]
        elif self.variables[var1.name] != self.variables[var2.name]:
            return False
        return True

    def val(self, var):
        return self.variables[var.name]

    def var_links(self, var):
        return self.links[self.variables[var.name]]

    def __str__(self):
        res = ""
        for const in self.links.keys():
            res += str(self.links[const]) + ": " + str(const) + "\n"
        return res


def unification(p1, p2):
    table = Table()
    if p1.name != p2.name:
        return False, table
    if len(p1.terminals) != len(p2.terminals):
        return False, table
    for t1, t2 in zip(p1.terminals, p2.terminals):
        if t1.variable:
            if t2.variable:
                if not table.equals_var(t1, t2):
                    print("Variable ", t1.name, "value mismatched another variable ", t2.name, ":", table.val(t1),
                          " != ", table.val(t2), sep='')
                    return False, table
            else:
                if not table.equals_const(t1, t2):
                    print("Variable value mismatched constant:", t1.name, "=", table.val(t1),
                          "but the constant is", t2.value)
                    return False, table
        else:
            if t2.variable:
                if not table.equals_const(t2, t1):
                    print("Variable value mismatched constant:", t2.name, "=", table.val(t2),
                          "but the constant is", t1.value)
                    return False, table
            else:
                if t1.value != t2.value:
                    print("Mismatch constants:", t1.value, "!=", t2.value)
                    return False, table
    return True, table


if __name__ == "__main__":
    v1 = Variable("v1")
    v2 = Variable("v2")
    c1 = Constant(1)
    a1 = Atom("A", [v1, c1, v1])
    a2 = Atom("A", [c1, v2, v2])
    b1 = Atom("B", [v1, c1, v1])
    res, table = unification(a1, a2)
    print(res)
    print(table)

